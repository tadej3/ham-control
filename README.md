# HAM Control

This project enables remote control of HAM radios through a mobile or web app. Initially it will be targeting the ICOM
IC-7300 radio, but can easily be expanded to support other radios in the future. At the moment, only one radio will be
supported per instance, but support for multiple radios might be added in the future.

> This project is currently in very early stages of development, so issues or feature requests will likely be ignored,
> until all of my initial ideas are implemented.

## Client

The `client` directory contains a [React Native for Web](https://necolas.github.io/react-native-web/) project which
builds the UI for controlling the radio remotely. It can be built into an Android, iOS, or web app.

## API

The `api` directory contains a [NestJS](https://nestjs.com/) project which should be deployed on e.g. a Raspberry Pi,
with a connection to the radio (in the case of IC-7300 this would be a single USB connection). It provides an interface
between the radio and `client` UI project.

## Raspberry Pi Custom Image

The `raspberry_pi` image contains all the tooling to build a custom Raspberry Pi OS (Bullseye) image.
