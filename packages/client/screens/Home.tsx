import { Text, View } from 'react-native';
import { theme } from '../src/theme';

export default function Home() {
  return (
    <View style={{ backgroundColor: theme.colors.primary, width: '100%', height: '100%' }}>
      <Text style={{ color: theme.colors.text }}>Test</Text>
    </View>
  );
}
