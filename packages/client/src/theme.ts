export const theme = {
  dark: true,
  colors: {
    primary: '#272D2D',
    secondary: '#5BC0EB',
    warning: '#FDE74C',
    success: '#9BC53D',
    error: '#C3423F',
    text: '#FFF9FB',
  },
};
