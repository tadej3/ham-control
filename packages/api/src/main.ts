/*import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';*/
import { RigCtl, RigMode, SIUnit, fromHertz, toHertz } from '@duplexsi/rigctljs';

async function bootstrap() {
  /*const app = await NestFactory.create(AppModule);
  await app.listen(3000);*/
  const rigctl = new RigCtl();
  await rigctl.connect();
  console.log('Connected');
  let freq = await rigctl.getFrequency();
  console.log('Initial frequency', fromHertz(freq, SIUnit.MEGA), 'MHz');
  await rigctl.setFrequency(toHertz(3.5, SIUnit.MEGA));
  freq = await rigctl.getFrequency();
  console.log('Frequency after update', fromHertz(freq, SIUnit.MEGA), 'MHz');
  let mode = await rigctl.getMode();
  console.log('Current mode', mode.Mode, 'Passband', fromHertz(Number(mode.Passband), SIUnit.KILO), 'kHz');
  await rigctl.setMode(RigMode.CW);
  await new Promise((resolve) => setTimeout(resolve, 500));
  mode = await rigctl.getMode();
  console.log('Mode after update', mode.Mode, 'Passband', fromHertz(Number(mode.Passband), SIUnit.KILO), 'kHz');
  await rigctl.disconnect();
  console.log('Disconnected');
}
bootstrap();
